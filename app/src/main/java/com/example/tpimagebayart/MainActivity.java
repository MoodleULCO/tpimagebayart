package com.example.tpimagebayart;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.FileNotFoundException;
import java.io.IOException;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    /**
     * Méthode permettant de charger une image dans l'application
     *
     * @param view
     */
    public void chargerImage(View view) {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        // --- on démarre une activité qui attend un résultat (ici l'image choisie) --- //
        startActivityForResult(Intent.createChooser(intent, "Choose Picture"), 1);
    }

    /**
     * Méthode appellée suite à la selection d'une image
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // --- Dans le cas où une image a été selectionnée --- //
        if (resultCode == RESULT_OK && requestCode == 1) {
            // --- Récupération de l'URI de l'image selectionnée --- //
            Uri selectedimg = data.getData();
            ImageView imageView = findViewById(R.id.imageView);

            // --- On affiche l'URI de l'image dans le TextView --- //
            TextView urlView = findViewById(R.id.urlView);
            urlView.setText(selectedimg.toString());

            // ----- préparer les options de chargement de l’image
            BitmapFactory.Options option = new BitmapFactory.Options();
            option.inMutable = true; // l’image pourra être modifiée
            // ------ chargement de l’image - valeur retournée null en cas d’erreur

            try {
                Bitmap bm = BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedimg), null, option);
                // --- On applique l'image selectionnée à l'ImageView de l'application --- //
                imageView.setImageBitmap(bm);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * Méthode appelée pour faire un effet miroir (horizontal ou vertical selon le bouton cliqué)
     *
     * @param view
     */
    public void applyMirror(View view) {
        ImageView image = findViewById(R.id.imageView);
        // --- On stocke l'image courante --- //
        Bitmap bitmap = ((BitmapDrawable)image.getDrawable()).getBitmap();

        switch(view.getId()) {
            case R.id.horizButton:
                // --- Dans le cas du clic sur le bouton "Miroir Horizontal", on échange les pixels de l'image "flipped" à l'aide de ceux de l'image courante" --- /
                // --- On affiche l'image "flipped" dans l'ImageView --- //
                image.setImageBitmap(horizontalFLip(bitmap));
                break;
            case R.id.verticButton:
                // --- Dans le cas du clic sur le bouton "Miroir Vertical", on échange les pixels de l'image "flipped" à l'aide de ceux de l'image courante" --- /
                // --- On affiche l'image "flipped" dans l'ImageView --- //
                image.setImageBitmap(verticalFlip(bitmap));
                break;
            default:
                break;
        }
    }

    /**
     * Méthode permettant de flip horizontalement une image
     *
     * @param init
     * @return l'image "flipped"
     */
    public Bitmap horizontalFLip(Bitmap init) {
        // --- On crée une image "flipped" qui sera le résultat de l'effet miroir --- //
        Bitmap flipped = Bitmap.createBitmap(init);

        for(int y = 0; y < init.getHeight(); y++) {
            for(int x = 0; x < init.getWidth(); x++) {
                // --- On échange le pixel avec celui qui est à l'opposé horizontalement --- //
                flipped.setPixel((init.getWidth()-1) - x, y, init.getPixel(x, y));
            }
        }
        return flipped;
    }

    /**
     * Méthode permettant de flip verticalement une image
     *
     * @param init
     * @return l'image "flipped"
     */
    public Bitmap verticalFlip(Bitmap init) {
        // --- On crée une image "flipped" qui sera le résultat de l'effet miroir --- //
        Bitmap flipped = Bitmap.createBitmap(init);

        for(int y = 0; y < init.getHeight(); y++) {
            for(int x = 0; x < init.getWidth(); x++) {
                // --- On échange le pixel avec celui qui est à l'opposé verticalement --- //
                flipped.setPixel(x, (init.getHeight()-1) - y, init.getPixel(x, y));
            }
        }
        return flipped;
    }
}